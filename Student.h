#include<iostream>
#include<string>
class Student
{
private:
	char ID[11];
	char Name[20];
	double math, physics, english;
public:
	Student(char*ID=NULL,char*Name= NULL, double math = 0,double  physics  = 0,double  english = 0);
	void setMessage(char* ID, char* Name,double  math,double phsics, double  english);
	void getMessage();
	char*  getID();
	char* getName();
	double  getMath();
	double getPhysics();
	double getEnglish();

};